import { Router } from 'express'

import RulesController from './controllers/RulesController'

const routes = Router()

routes.get('/rules', RulesController.index)
routes.get('/schedules', RulesController.schedulesAvailable)
routes.post('/rules', RulesController.create)
routes.delete('/rules/:day', RulesController.destroy)

export default routes
