import { Request, Response } from 'express'

import Rule from '../models/RuleModel'

class RulesController {
  public async index (req: Request, res: Response): Promise<Response> {
    const rules = await Rule.getRules()

    return res.json(rules)
  }

  public async schedulesAvailable (req: Request, res: Response): Promise<Response> {
    const rules = await Rule.schedulesAvailable(req.body)

    return res.json(rules)
  }

  public async create (req: Request, res:Response): Promise<Response> {
    const rule = await Rule.create(req.body)

    return res.json(rule)
  }

  public async destroy (req: Request, res:Response): Promise<Response> {
    const rule = await Rule.destroy(req.params.day)

    return res.json(rule)
  }
}

export default new RulesController()
