/* eslint-disable @typescript-eslint/explicit-function-return-type */
import lowdb from 'lowdb'
import FileSync from 'lowdb/adapters/FileSync'
import moment from 'moment'

class RuleModel {
  private rules

  public constructor () {
    this.initDatabase()
  }

  private initDatabase () : void {
    const adapter = new FileSync('db.json')
    let db = lowdb(adapter)

    db.defaults({ rules: [] }).write()

    this.rules = db.get('rules')
  }

  public async getRules (): Promise<string> {
    const rules = await this.rules.value()
    return rules
  }

  public async schedulesAvailable (date): Promise<string> {
    let firstDate = moment(date.first, 'DD-MM-YYYY', true)
    let lastDate = moment(date.last, 'DD-MM-YYYY', true)

    if (!firstDate.isValid() || !lastDate.isValid()) {
      return 'Formato inserido inválido'
    }

    let schendules = this.rules.value().filter(rule => {
      let date = moment(rule.day, 'DD-MM-YYYY', true)
      if (date.isValid()) {
        return date.isSameOrAfter(firstDate) && date.isSameOrBefore(lastDate)
      }
    })

    return schendules
  }

  public async create (rule) : Promise<string> {
    let day = this.validateDay(rule.day)

    if (day === 'invalid') {
      return 'Formato de Dia inválido'
    }

    let data = {
      day: day,
      intervals: rule.intervals
    }

    let existRule = await this.existRule(data)

    switch (existRule) {
      case 'justDay':
        const updateRules = data.intervals.map(interval =>
          this.rules.find({ day: data.day })
            .get('intervals').push(interval).write())
        return updateRules
      case 'notExist':
        const rules = await this.rules.push(data).write()
        return rules
      case 'justWeek':
        return 'Regra com semana Já existe, exclua e cadastre novamente'
      default:
        return 'Regra já existente'
    }
  }

  public async destroy (data) : Promise<string> {
    let day = this.validateDay(data)

    const rules = await this.rules.remove({ day: day }).write()
    return rules
  }

  private async existRule (rule) : Promise<string> {
    const existWeek = await this.rules.value()
    const existDay = await this.rules.find({ day: rule.day }).value()
    const rules = await this.rules.find(rule).value()

    if (rules) {
      return 'exist'
    } else if (Array.isArray(rule.day) && Array.isArray(existWeek)) {
      return 'justWeek'
    } else if (existDay) {
      return 'justDay'
    } else {
      return 'notExist'
    }
  }

  private validateDay (day) : any {
    moment.locale('pt_br')
    let date = moment(day, 'DD-MM-YYYY', true)

    if (date.isValid()) {
      return day
    } else if (Array.isArray(day)) {
      let days = [...new Set(day)]

      let weekdays = days.filter(a => Number.isInteger(a) && (a >= 0 && a <= 7))
        .map(data => {
          if (Number.isInteger(data) && (data >= 0 && data <= 7)) {
            return moment.weekdays(parseInt(data))
          }
        })
      return weekdays
    } else if (Number.isInteger(parseInt(day))) {
      return moment.weekdays(parseInt(day))
    } else if (day === 'diaria') {
      return 'diariamente'
    } else {
      return 'invalid'
    }
  }
}

export default new RuleModel()
