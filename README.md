## Primeiros passos

Pré-requisitos: [Git](http://git-scm.com/downloads) e o [NodeJS](http://nodejs.org/download/)

2. Clone o repositório:

    ```sh
    $ git clone https://gitlab.com/swlucas/desafio-backend
    ```
3. Vá para pasta do projeto:

    ```sh
    $ cd desafio-backend
    ```

4. Instale todas as dependências:

    ```sh
    $ npm install ou yarn
    ```

5. E finalmente rode:

    ```sh
    $ node src/server.ts
    ```
   O site estará rodando em `localhost:3000`

## Rotas

As rotas estão disponíveis aqui: [Rotas](https://documenter.getpostman.com/view/4757386/S1EJYMkb#0db9b315-8518-4b42-ac7f-8b0d8704adbf "Rotas")

